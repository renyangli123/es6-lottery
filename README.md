# es6-lottery
es6 彩票项目的实战，其中运用到es6的新特性
运用到新特性有，解构赋值，模板字符串，set和map的数据结构，Promise,Reflect，模块化import 和export
功能模块包括倒计时，玩法切换，计算金额，添加号码，投注支付，随机号码，奖金预测，状态更新，动态遗漏

构建工具： gulp+babel+webpack+npm
初始化项目
sudo npm install

cd server && npm install

启动服务器
gulp-watch
